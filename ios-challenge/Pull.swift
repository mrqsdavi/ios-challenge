//
//  Pull.swift
//  ios-challenge
//
//  Created by Davi on 07/05/17.
//  Copyright © 2017 Davi. All rights reserved.
//

import Foundation
import Argo
import Curry
import Runes

struct Pull {
    let id: Int
    let title: String
    let url: String
    let body: String
    let state: String
    var owner: Owner
}

extension Pull: Decodable {
    static func decode(_ json: JSON) -> Decoded<Pull> {
        return curry(Pull.init)
            <^> json <| "id"
            <*> json <| "title"
            <*> json <| "html_url"
            <*> json <| "body"
            <*> json <| "state"
            <*> json <| "user"
    }
}
