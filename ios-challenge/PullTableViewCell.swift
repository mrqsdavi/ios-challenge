//
//  PullTableViewCell.swift
//  ios-challenge
//
//  Created by Davi on 07/05/17.
//  Copyright © 2017 Davi. All rights reserved.
//

import UIKit

class PullTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel?
    @IBOutlet weak var bodyLabel: UILabel?
    @IBOutlet weak var usernameLabel: UILabel?
    @IBOutlet weak var ownernameLabel: UILabel?
    @IBOutlet weak var ownerImageView: UIImageView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
