//
//  PullsTableViewController.swift
//  ios-challenge
//
//  Created by Davi on 07/05/17.
//  Copyright © 2017 Davi. All rights reserved.
//

import UIKit

class PullsTableViewController: UITableViewController {
    
    var repository: Repository?
    var pulls: [Pull]? = nil
    var opened = 0
    var closed = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = self.repository?.name
        self.navigationController?.navigationBar.topItem?.title = "";
    }
    
    func loadMore(){
        GitHubAPI.sharedInstance.loadPulls(repository: (repository?.name)!, user: (repository?.owner.login)!, completion: { result in
            self.pulls = result
            
            self.opened = (self.pulls?.filter { (pull : Pull) -> Bool in
                return pull.state == "open"
            }.count)!
            self.closed = (self.pulls?.filter { (pull : Pull) -> Bool in
                return pull.state == "closed"
            }.count)!
            
            
            self.tableView.reloadData()
            
            for i in 0 ..< (self.pulls?.count)! {
                let pull = self.pulls?[i]
                GitHubAPI.sharedInstance.loadUser(user: (pull?.owner.login)!, completion: {usuario in
                    if(usuario != nil){
                        self.tableView.beginUpdates()
                        self.pulls?[i].owner = usuario!
                        let indexPath = IndexPath(row: i, section: 0)
                        self.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)
                        self.tableView.endUpdates()
                    }
                })
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        if(pulls == nil){
            return 1
        }
        
        return (pulls?.count)!
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if(pulls == nil){
            return 44
        }
        
        return 150;
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(pulls == nil){
            let cellLoadMore = tableView.dequeueReusableCell(withIdentifier: "loadmore", for: indexPath)
            return cellLoadMore;
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "pullcell", for: indexPath) as! PullTableViewCell

        let item = self.pulls?[indexPath.row]
        cell.nameLabel?.text = item?.title
        cell.bodyLabel?.text = item?.body
        cell.usernameLabel?.text = item?.owner.login
        cell.ownerImageView?.sd_setImage(with: URL(string: (item?.owner.avatar_url)!))
        cell.ownernameLabel?.text = item?.owner.name == nil ? "" : item?.owner.name

        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "headercell") as! PullTableViewCell
        cell.nameLabel?.text = "\(self.opened) opened"
        cell.bodyLabel?.text = "/ \(self.closed) closed"
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let item = self.pulls?[indexPath.row]
        let url = URL(string: (item?.url)!)!
        UIApplication.shared.open(url)
    }
    
    
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if(pulls == nil){
            loadMore()
        }
    }

}
