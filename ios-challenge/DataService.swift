//
//  DataService.swift
//  ios-challenge
//
//  Created by Davi on 06/05/17.
//  Copyright © 2017 Davi. All rights reserved.
//

import Foundation
import Moya

enum DataService {
    case loadRepositories(q: String, page: Int)
    case loadPulls(repository: String, user: String)
    case loadUser(user: String)
}

extension DataService: TargetType {
    var baseURL: URL { return URL(string: "https://api.github.com")! }
    var path: String {
        switch self {
        case .loadRepositories:
            return "/search/repositories"
        case .loadPulls(let repository, let user):
            return "/repos/\(user)/\(repository)/pulls"
        case .loadUser(let user):
            return "/users/\(user)"
        }
    }
    var method: Moya.Method {
        switch self {
        case .loadRepositories, .loadPulls, .loadUser:
            return .get
        }
    }
    var parameters: [String: Any]? {
        switch self {
        case .loadRepositories(let q, let page):
            return ["q": q, "sort": "starts", "page": page]
        case .loadPulls, .loadUser:
            return nil
        }
    }
    
    var parameterEncoding: ParameterEncoding {
        switch self {
        case .loadRepositories, .loadPulls, .loadUser:
            return URLEncoding.default
        }
    }
    var sampleData: Data {
        switch self {
        case .loadRepositories:
            return "Repositories.".utf8Encoded
        case .loadPulls:
            return "Pulls.".utf8Encoded
        case .loadUser:
            return "User.".utf8Encoded
        }
    }
    var task: Task {
        switch self {
        case .loadRepositories, .loadPulls, .loadUser:
            return .request
        }
    }
}
// MARK: - Helpers
private extension String {
    var urlEscaped: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return self.data(using: .utf8)!
    }
}
