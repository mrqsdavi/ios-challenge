//
//  RepositoriesTableViewController.swift
//  ios-challenge
//
//  Created by Davi on 06/05/17.
//  Copyright © 2017 Davi. All rights reserved.
//

import UIKit
import SDWebImage

class RepositoriesTableViewController: UITableViewController {
    
    var currentPage = 0;
    var totalRecords = 0;
    var totalLoadedRecords = 0;
    var records: [Repository]? = nil;

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title = "Swift Repositories";
    }
    
    func loadMore(){
        self.currentPage += 1
        GitHubAPI.sharedInstance.loadSwiftRepositories(page: self.currentPage, completion: {result in
            
            let last = self.totalLoadedRecords
            
            
            if(self.records == nil){
                self.records = [Repository]()
            }
            
            if(result.count > 0){
                self.totalRecords = result[0].total!
            }
            
            self.records?.append(contentsOf: result)
            self.totalLoadedRecords = (self.records?.count)!
            
            self.tableView.reloadData()
            
            for i in 0 ..< result.count {
                let rep = self.records?[i+last]
                GitHubAPI.sharedInstance.loadUser(user: (rep?.owner.login)!, completion: {usuario in
                    if(usuario != nil){
                        self.tableView.beginUpdates()
                        self.records?[i+last].owner = usuario!
                        let indexPath = IndexPath(row: i + last, section: 0)
                        self.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)
                        self.tableView.endUpdates()
                    }
                })
            }
        });
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numberOfRows = totalLoadedRecords;
        if(records == nil || totalLoadedRecords < totalRecords){
            numberOfRows += 1;
        }
        return numberOfRows;
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if(indexPath.row >= totalLoadedRecords){
            return 44;
        }
        
        return 140;
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.row >= totalLoadedRecords){
            let cellLoadMore = tableView.dequeueReusableCell(withIdentifier: "loadmore", for: indexPath)
            return cellLoadMore;
        }
        
        let item: Repository = self.records![indexPath.row]
        
        let cell: RepositoryTableViewCell = tableView.dequeueReusableCell(withIdentifier: "repositorycell", for: indexPath) as! RepositoryTableViewCell
        
        cell.tag = indexPath.row
        cell.nameLabel?.text = item.name
        cell.descriptionLabel?.text = item.description
        cell.forksLabel?.text = "\(item.forks)"
        cell.starsLabel?.text = "\(item.stargazers_count)"
        cell.usernameLabel?.text = item.owner.login
        cell.ownernameLabel?.text = item.owner.name == nil ? "" : item.owner.name
        cell.ownerImageView?.sd_setImage(with: URL(string: item.owner.avatar_url))
        
        let iconColor = UIColor(red:0.87, green:0.57, blue:0.12, alpha:1.0)
        
        cell.forkImageView?.image = cell.forkImageView?.image!.withRenderingMode(.alwaysTemplate)
        cell.forkImageView?.tintColor = iconColor
        
        cell.starsImageView?.image = cell.starsImageView?.image!.withRenderingMode(.alwaysTemplate)
        cell.starsImageView?.tintColor = iconColor
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if(indexPath.row >= totalLoadedRecords){
            loadMore()
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "pullsegue"){
            let cellSender = sender as! UITableViewCell
            let pullTableViewController : PullsTableViewController = segue.destination as! PullsTableViewController
            pullTableViewController.repository = records?[cellSender.tag]
        }
    }

}
