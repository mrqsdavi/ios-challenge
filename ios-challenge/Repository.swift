//
//  Repository.swift
//  ios-challenge
//
//  Created by Davi on 06/05/17.
//  Copyright © 2017 Davi. All rights reserved.
//

import Foundation
import Argo
import Curry
import Runes

struct Repository {
    let id: Int
    let name: String
    let description: String
    let full_name: String
    let forks: Int
    let stargazers_count: Int
    var owner: Owner
    var total: Int?
}

extension Repository: Decodable {
    static func decode(_ json: JSON) -> Decoded<Repository> {
        return curry(Repository.init)
            <^> json <| "id"
            <*> json <| "name"
            <*> json <| "description"
            <*> json <| "full_name"
            <*> json <| "forks"
            <*> json <| "stargazers_count"
            <*> json <| "owner"
            <*> json <|? "total"
    }
}
