//
//  RepositoryTableViewCell.swift
//  ios-challenge
//
//  Created by Davi on 06/05/17.
//  Copyright © 2017 Davi. All rights reserved.
//

import UIKit

class RepositoryTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel?
    @IBOutlet weak var descriptionLabel: UILabel?
    @IBOutlet weak var forksLabel: UILabel?
    @IBOutlet weak var starsLabel: UILabel?
    @IBOutlet weak var usernameLabel: UILabel?
    @IBOutlet weak var ownernameLabel: UILabel?
    @IBOutlet weak var ownerImageView: UIImageView?
    @IBOutlet weak var forkImageView: UIImageView?
    @IBOutlet weak var starsImageView: UIImageView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
