//
//  GitHubAPI.swift
//  ios-challenge
//
//  Created by Davi on 06/05/17.
//  Copyright © 2017 Davi. All rights reserved.
//

import Foundation
import Moya
import Argo

class GitHubAPI {
    
    static let sharedInstance : GitHubAPI = {
        let instance = GitHubAPI()
        return instance
    }()
    
    let provider = MoyaProvider<DataService>()
    
    func loadSwiftRepositories(page: Int, completion: @escaping (_ result: [Repository]) -> Void){
        provider.request(.loadRepositories(q: "language:Swift", page: page)) { result in
            switch result {
            case let .success(moyaResponse):
                let data = moyaResponse.data
                var result = [Repository]()
                
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments) as! [String:AnyObject]
                    
                    let total: Int = json["total_count"] as! Int
                    let items: [Any] = json["items"] as! [Any]
                    
                    for case let j in items{
                        var repository: Repository? = decode(j)
                        repository?.total = total
                        result.append(repository!)
                    }
                } catch let err{
                    print(err.localizedDescription)
                }
                
                completion(result);
                
            // do something in your app
            case .failure(_):
                completion([Repository]())
            }
        }
    }
    
    func loadPulls(repository: String, user: String, completion: @escaping (_ result: [Pull]) -> Void){
        provider.request(.loadPulls(repository: repository, user: user)) { result in
            switch result {
            case let .success(moyaResponse):
                let data = moyaResponse.data
                var result = [Pull]()
                
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments) as! [AnyObject]
                    
                    for case let j in json{
                        let pull: Pull? = decode(j)
                        result.append(pull!)
                    }
                } catch let err{
                    print(err.localizedDescription)
                }
                
                completion(result);
                
            // do something in your app
            case .failure(_):
                completion([Pull]())
            }
        }
    }
    
    func loadUser(user: String, completion: @escaping (_ result: Owner?) -> Void){
        provider.request(.loadUser(user: user)) { result in
            switch result {
            case let .success(moyaResponse):
                let data = moyaResponse.data
                var result : Owner? = nil
                
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments) as! [String:AnyObject]
                    //let dataString = String(data:data, encoding:String.Encoding.utf8)
                    let owner: Owner? = decode(json)
                    result = owner
                } catch let err{
                    print(err.localizedDescription)
                }
                
                completion(result);
                
            // do something in your app
            case .failure(_):
                completion(nil)
            }
        }
    }

}
