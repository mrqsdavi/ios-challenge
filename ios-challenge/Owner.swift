//
//  Owner.swift
//  ios-challenge
//
//  Created by Davi on 07/05/17.
//  Copyright © 2017 Davi. All rights reserved.
//

import Foundation
import Argo
import Curry
import Runes

struct Owner {
    let id: Int
    let login: String
    let avatar_url: String
    let name: String?
}

extension Owner: Decodable {
    static func decode(_ json: JSON) -> Decoded<Owner> {
        return curry(Owner.init)
            <^> json <| "id"
            <*> json <| "login"
            <*> json <| "avatar_url"
            <*> json <|? "name"
    }
}
